# maxperfwiz

maxperfwiz is a script that assists the user in configuring a selection of parameters with the purpose of increasing system performance.

### Download:

```shell
curl -O https://gitlab.com/cscs/maxperfwiz/raw/master/maxperfwiz
```

### Mark Executable:

```shell
chmod +x maxperfwiz
```

### How to use:

Simply follow the prompts. 
Recommended values will be presented and applied unless the '--custom-values' flag is used.

Example first run:
```shell
./maxperfwiz

maxperfwiz is a script that assists the user in configuring a selection of 
kernel parameters with the purpose of increasing system performance.

All changes to the configurations are first written to temporary files:
     /tmp/maxperfwiz/99-maxperfwiz.conf
     /tmp/maxperfwiz/66-maxperfwiz.rules

When customizations are complete you will be prompted to apply them. The 
following files will be affected:
     /etc/sysctl.d/99-maxperfwiz.conf
     /etc/udev/rules.d/66-maxperfwiz.rules

To remove these files and undo all changes applied by this wizard run
     ./maxperfwiz --remove

Modify sysctl tweaks (Y/n)? 

Modify udev tweaks (Y/n)? 

..cleaning up by removing /tmp/maxperfwiz
```

### created by cscs and <a href='https://forum.manjaro.org/u/kresimir'>kresimir</a>

> Note: This is a work in progress. 

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
